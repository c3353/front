import { Category } from "./category";


export class StandardProduct {
    qteAvailable :number;
    picture: string;
    description:string;
    category:Category;
    id: number;
    price: number;
    text:string;
}
