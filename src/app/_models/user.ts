import { Role } from "./role";

export class User {
    id: number;
    email: string;
    password: string;
    firstname: string;
    lastname: string;
    roles: Role;
    token?: string;
}