import { Inject, Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { environment } from '../../environment';
import { BehaviorSubject, Observable} from 'rxjs';
import { StandardProduct } from '../_models/standardProduct';
import { map } from 'rxjs/operators';
import { Category } from '../_models/category';


@Injectable({
  providedIn: 'root'
})
export class ProductService {

  private messageSource = new BehaviorSubject(null);
  currentMessage = this.messageSource.asObservable();

 
    constructor(
      private http: HttpClient
  ) {}



  getStandardProducts(){
    return this.http.get(`${environment.apiUrl}/standardProducts`)
    .pipe(map(res=> <StandardProduct[]> res))
  }

  getCategories(){
    return this.http.get(`${environment.apiUrl}/getAllCategories`)
    .pipe(map(res=> <Category[]> res))
  }

  addProduct(product){
    return this.http.post<any>(`${environment.apiUrl}/standardProduct`, product);
  }

 

  changeMessage(message) {
    this.messageSource.next(message)
  }

  deleteProduct(id :number){
    return this.http.delete(`${environment.apiUrl}/deleteStandardProduct/${id}`)
  }

}
