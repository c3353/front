import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { environment } from '../../environment';
import { User } from '../_models';
import { map } from 'rxjs/operators';
import { Router } from '@angular/router';
import { BehaviorSubject, Observable, throwError } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class UserService {

   
    private userSubject: BehaviorSubject<User>;
    private currentUserSubject: BehaviorSubject<User>;
    public user: Observable<User>;
   

    constructor(
        private router: Router,
        private http: HttpClient,
    ) {
        this.userSubject = new BehaviorSubject<User>(JSON.parse(localStorage.getItem('user')));
        this.currentUserSubject = new BehaviorSubject<User>(JSON.parse(localStorage.getItem('currentUser')));
        this.user = this.userSubject.asObservable();
    }

    public get userValue(): User {
        return this.userSubject.value;
    }
    public get getcurrentUser(): User {
        return this.currentUserSubject.value;
    }
    getAll() {
        return this.http.get<User[]>(`${environment.apiUrl}/user/searchAll`);
    }
 
    getById(id: number) {
        return this.http.get<User>(`${environment.apiUrl}/user/${id}`);
    }
    register(user: User) {
        return this.http.post<any>(`${environment.apiUrl}/user/add`, {...user, roles:["client"]})
            ;
    }
    currentUser(){
        return this.http.get<User>(`${environment.apiUrl}/currentUser`).pipe(map(user => {
            // store user details and jwt token in local storage to keep user logged in between page refreshes
            localStorage.setItem('currentUser', JSON.stringify(user));
            this.currentUserSubject.next(user);
            return user;
        }));

    }
    update(id, params) {
        return this.http.put<any>(`${environment.apiUrl}/user/update/${id}`, params)
            .pipe(map(x => {
                // update stored user if the logged in user updated their own record
                if (id == this.userValue.id) {
                    // update local storage
                    const user = { ...this.userValue, ...params };
                    localStorage.setItem('user', JSON.stringify(user));

                    // publish updated user to subscribers
                    this.userSubject.next(user);
                }
                return x;
            }));
    }
    logout() {
        // remove user from local storage and set current user to null
        localStorage.removeItem('user');
        this.userSubject.next(null);
        this.router.navigate(['/login']);
    }

    delete(id: number) {
        return this.http.delete(`${environment.apiUrl}/user/delete/${id}`)
            .pipe(map(x => {
                // auto logout if the logged in user deleted their own record
                if (id == this.userValue.id) {
                    this.logout();
                }
                return x;
            }));
    }

    getUserCart(id: number) {
        return this.http.get<any[]>(`${environment.apiUrl}/cartListByUserId/${id}`);
    }

    //Update product qte from cart
    updateQte(body?: any): Observable<any> {
        // verify required parameters are not null or undefined
        if (body === null || body === undefined) {
          return throwError("Required parameter body was null or undefined.");
        }
        const headers = new HttpHeaders({ "Content-Type": "application/json" });
    
        return this.http.post<any>(
            `${environment.apiUrl}/UpdateQuantityProduct/${body.id}/${body.product.price}`,
          body,
          { headers: headers }
        );
      }
      //Delete product from cart
      deleteProductFromCart(userId,productId): Observable<any> {
        return this.http.delete(`${environment.apiUrl}/deleteProductFromCart/${userId}/${productId}`);
      }
    
        getUserOrder(id: number) {
        return this.http.get<any[]>(`${environment.apiUrl}/orderListByUserId/${id}`);
    }
}

