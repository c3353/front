import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddStandardProductDialogComponent } from './add-standard-product-dialog.component';

describe('AddStandardProductDialogComponent', () => {
  let component: AddStandardProductDialogComponent;
  let fixture: ComponentFixture<AddStandardProductDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddStandardProductDialogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddStandardProductDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
