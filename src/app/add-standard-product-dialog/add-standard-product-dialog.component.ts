import { Component, OnInit } from '@angular/core';
import { ProductService } from '../_services/product.service';

import {
  MatSnackBar,
  MatSnackBarHorizontalPosition,
  MatSnackBarVerticalPosition,
} from '@angular/material/snack-bar';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-add-standard-product-dialog',
  templateUrl: './add-standard-product-dialog.component.html',
  styleUrls: ['./add-standard-product-dialog.component.css']
})
export class AddStandardProductDialogComponent implements OnInit {

  constructor(private productService :ProductService,
    private _snackBar: MatSnackBar) { }

  ngOnInit(): void {
    this.getCategories()
    this.subscription = this.productService.currentMessage.subscribe()
  }
  horizontalPosition: MatSnackBarHorizontalPosition = 'end';
  verticalPosition: MatSnackBarVerticalPosition = 'bottom';

  subscription: Subscription;

  categoryList : any
  selectedOption : any
  price = 0
  description=""
  qte=0
  fileName=""


  onFileChanged(event) {
     this.fileName = event.target.files[0].name
  }


  getCategories() : void{
    this.productService.getCategories()
    .subscribe(categories => {
     this.categoryList = categories;
    });

  }

  addStandardProduct(){
    var standardProduct = { 
    qteAvailable: this.qte,
    picture: this.fileName,
    description: this.description,
    price: this.price,
    text: "",
    category: this.selectedOption}
    this.productService.addProduct(standardProduct).subscribe(()=>{
      this.productService.getStandardProducts().subscribe(
        standardProducts => {
          this.productService.changeMessage(standardProducts)
          this._snackBar.open('Product added', 'Close', {
            horizontalPosition: this.horizontalPosition,
            verticalPosition: this.verticalPosition,
            duration: 3000,
          });
        }
      )
     

    })
  }
  
}
