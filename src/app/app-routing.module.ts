import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './home';
import { ListComponent } from './userManegement/userManegement';
import { LoginComponent } from './login';
import { ProductManagementComponent } from './product-management/product-management.component';
import { RegisterComponent } from './register';
import { AuthGuard } from './_helpers';
import { AddEditComponent } from './userManegement/editUser';
import { Role } from './_models';
import { CardComponent } from './card/card.component';
import { RequestdProductComponent } from './requestd-product/requestd-product.component';

const routes: Routes = [
  {
    path: '',
    component: HomeComponent,
  },
  {
    path: 'userManegement',
    component: ListComponent,
    data: { roles: [Role.Admin] },
  },
  {
    path: 'editUser',
    component: AddEditComponent,
  },
  {
    path: 'requestProduct',
    component: RequestdProductComponent,
  },
  {
    path: 'editUser/:id',
    component: AddEditComponent,
  },
  {
    path: 'register',
    component: RegisterComponent,
  },
  {
    path: 'login',
    component: LoginComponent,
  },
  {
    path: 'cart',
    component: CardComponent,
    //canActivate: [AuthGuard]
  },

    {
        path: 'productManagement',
        component: ProductManagementComponent,
        canActivate: [AuthGuard],
      // data: { roles: [Role.Admin] }
    },

  // otherwise redirect to home
  { path: '**', redirectTo: '' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
