import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from './_services';
import { AuthenticationService } from './_services';
import { User, Role } from './_models';

@Component({
  selector: 'app',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.css'],
})
export class AppComponent {
  user: User;

  constructor(
    private userservice: UserService,
    private router: Router,
    private authenticationService: AuthenticationService
  ) {
    this.authenticationService.user.subscribe((x) => (this.user = x));
  }

  isAdmin() {
    return this.userservice.getcurrentUser.roles[0] === 'admin';
  }

  logout() {
    this.authenticationService.logout();
    this.router.navigate(['/login']);
  }
}
