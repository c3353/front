import { Component, OnInit } from '@angular/core';
import { first } from 'rxjs/operators';
import { UserService } from '../_services';



@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.css']
})
export class CardComponent implements OnInit {
  userCart = []
  qte : number
  updatedCart : any

  constructor(private userService:UserService) {}

  ngOnInit() {
    //this.userCart=await 
    this.userService.getUserCart(this.userService.getcurrentUser.id).pipe(first()).subscribe(userCart => this.userCart = userCart);
    console.log(this.userCart)
  }

  editAction(){
   
    this.updatedCart.qte = this.qte
    this.userService.updateQte(this.updatedCart).subscribe(e=>{
      console.log(e)
    })
  }

  deleteAction(cart){
    console.log(cart)
    this.userService.deleteProductFromCart(cart.user.id,cart.product.id).subscribe(e=>{
      console.log(e)
    })
  }

  display: boolean = false;

  showDialog(cart) {
      this.display = true;
      this.qte=cart.qte
      this.updatedCart=cart
  }

}
