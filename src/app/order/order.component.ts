import { Component, OnInit } from '@angular/core';
import { first } from 'rxjs/operators';
import { UserService } from '../_services';

@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.css']
})
export class OrderComponent implements OnInit {

  userOrder = []
  qte : number
  updatedOrder : any

  constructor(private userService:UserService) { }

  ngOnInit(): void {
    this.userService.getUserOrder(this.userService.getcurrentUser.id).pipe(first()).subscribe(userOrder => this.userOrder = userOrder);
  }

  editAction(){
   
    this.updatedOrder.qte = this.qte
    this.userService.updateQte(this.updatedOrder).subscribe(e=>{
      console.log(e)
    })
  }

  display: boolean = false;

  showDialog(order) {
      this.display = true;
      this.qte=order.qte
      this.updatedOrder=order
  }
  
}
