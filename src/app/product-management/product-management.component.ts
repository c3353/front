import { Component, AfterViewInit ,ViewChild, OnInit} from '@angular/core';
import {MatSort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import {MatDialog} from '@angular/material/dialog';
import { AddStandardProductDialogComponent } from "../add-standard-product-dialog/add-standard-product-dialog.component";

import {
  MatSnackBar,
  MatSnackBarHorizontalPosition,
  MatSnackBarVerticalPosition,
} from '@angular/material/snack-bar';


import { ProductService } from "../_services/product.service";
import { Subscription } from 'rxjs';



@Component({
  selector: 'app-product-management',
  templateUrl: './product-management.component.html',
  styleUrls: ['./product-management.component.css']
})
export class ProductManagementComponent implements OnInit,AfterViewInit {
  
  subscription: Subscription;

  constructor(public dialog: MatDialog,
    private productService :ProductService,
    private _snackBar: MatSnackBar) { }

    
    horizontalPosition: MatSnackBarHorizontalPosition = 'end';
  verticalPosition: MatSnackBarVerticalPosition = 'bottom';
  ngOnInit(): void {
    this.getStandardProducts()
    this.subscription = this.productService.currentMessage.subscribe((data) => this.dataSource.data=data)
  }

  displayedColumns: string[] = ['id','category', 'price',  'qteAvailable','picture','description','edit','delete'];
  dataSource = new MatTableDataSource();
  getStandardProducts() : void {
    this.productService.getStandardProducts()
    .subscribe(standardProducts => {
      this.dataSource.data = standardProducts;
    });
    
    
  }


  @ViewChild(MatSort) sort: MatSort;

  ngAfterViewInit(): void {
    this.dataSource.sort = this.sort;
  }

  addStandardProduct() {
    this.dialog.open(AddStandardProductDialogComponent);
  }
  deleteStandardProduct(id){
    this.productService.deleteProduct(id).subscribe(()=>{
      this.getStandardProducts()
      this._snackBar.open('Product deleted', 'Close', {
        horizontalPosition: this.horizontalPosition,
        verticalPosition: this.verticalPosition,
        duration: 3000,
      });
    }  
    )
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }


}
