import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RequestdProductComponent } from './requestd-product.component';

describe('RequestdProductComponent', () => {
  let component: RequestdProductComponent;
  let fixture: ComponentFixture<RequestdProductComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RequestdProductComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RequestdProductComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
