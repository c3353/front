import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-requestd-product',
  templateUrl: './requestd-product.component.html',
  styleUrls: ['./requestd-product.component.css'],
})
export class RequestdProductComponent implements OnInit {
  fileToUpload: File | null = null;

  constructor() {}

  ngOnInit(): void {}

  handleFileInput(files: FileList) {
    this.fileToUpload = files.item(0);
  }
}
